<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Funções</title>
    <?php
        function cabecalho($disc)
        {
            echo "<H1>COTIL 2016</H1>";
            echo "<H1>INFORMÁTICA DIURNO</H1>";
            echo "<h1>$disc</h1>";
        }
        function soma($x,$y)
        {
            return $x+$y;
        }
    ?>
</head>

<body>
    <?php
        cabecalho("Desenvolvimento de Aplicações WEB");
    ?>
    <h2>Soma 2+2 = <?php echo soma(2,2);?></h2>
</body>
</html>