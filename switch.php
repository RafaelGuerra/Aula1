<meta charset="UTF-8">
   <?php
    $x = 10;
    $y = 3;
    $op = "+";
    $res = 0.0;

    switch($op)
    {
        case "+":
            $res = $x+$y;
            break;
        case "-":
            $res = $x-$y;
            break;
        case "*":
            $res = $x*$y;
            break;
        case "/":
            $res = $x/$y;
            break;
        default:
            $res = "Operador inválido!";
            break;
    }
    echo "Resultado: ".$res;
?>