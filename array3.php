<meta charset="UTF-8">
<?php
$estados = array(
    0 => "Parado",
    827 => "Executando",
    2 => "Finalizado",
    "ES" => "Espírito Santo",
    "SP" => "São Paulo",
    "RJ" => "Rio de Janeiro"
);

foreach($estados as $chave => $valor)
    echo $chave, " = ", $valor, "<br>";